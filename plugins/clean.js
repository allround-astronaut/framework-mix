const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = function(config) {
    return new CleanWebpackPlugin({
        cleanStaleWebpackAssets: false,
        cleanOnceBeforeBuildPatterns: ['*/*', '!storage/*', ...config.ignoreClean],
    });
};