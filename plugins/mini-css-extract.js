const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = function(config) {
    return new MiniCssExtractPlugin({
        filename: config.versioning ? '[name].[contenthash:8].css' : '[name].css',
        chunkFilename: config.versioning ? '[name].[contenthash:8].css' : '[name].css',
    });
};
