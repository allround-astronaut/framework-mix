const WebpackBuildNotifierPlugin = require('webpack-build-notifier');

module.exports = function(config) {
    return new WebpackBuildNotifierPlugin({
        title: 'Mix',
        notifyOptions: {
            timeout: 2,
        }
    });
};