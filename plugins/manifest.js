const { WebpackManifestPlugin  } = require('webpack-manifest-plugin');

module.exports = function(config) {
    let result = {};

    for (const manifest of config.manifests) {
        Object.assign(result, require(process.cwd() + manifest));
    }

    return new WebpackManifestPlugin({
        publicPath: '',
        writeToFileEmit: true,
        seed: result
    });
};
