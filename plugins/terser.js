const TerserWebpackPlugin = require('terser-webpack-plugin');

module.exports = function(config) {
    return new TerserWebpackPlugin({
        parallel: true
    });
};
