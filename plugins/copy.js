const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = function(config) {
    let patterns = [];

    for (const [input, [output, ignore]] of config.copies) {
        patterns.push({
            from: input,
            to: output,
            noErrorOnMissing: true,
            globOptions: {
                ignore: ignore,
            },
            info: (file) => ({ sourceFilename: file.name })
        });    
    }

    return new CopyWebpackPlugin({patterns});
};
