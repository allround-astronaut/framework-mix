class Mix {
    constructor() {
        this.builder = new (require('./builder'))();
    }

    entry(input, output) {
        this.builder.entry(input, output);
        return this;
    }

    copy(input, output = null, ignore = []) {
        this.builder.copy(input, output, ignore);
        return this;
    }

    ignoreClean(ignore) {
        this.builder.ignoreClean(ignore);
        return this;
    }

    addManifest(manifest) {
        this.builder.addManifest(manifest);
        return this;
    }

    enableVersioning(versioning = true) {
        this.builder.enableVersioning(versioning);
        return this;
    }

    isProduction() {
        return this.builder.config.production;
    }

    generate() {
        return this.builder.generate();
    }
}

module.exports = new Mix();
