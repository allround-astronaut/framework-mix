const path = require('path');

class Config {
    constructor() {
        this.entries = new Map();
        this.copies = new Map();
        this.production = process.env.production ? true : false;
        this.ignoreClean = [];
        this.manifests = [];
        this.versioning = false;
        this.paths = {
            public: process.cwd() + '/public/',
            root: path.resolve(__dirname) + '/'
        }
    }
}

module.exports = Config;
