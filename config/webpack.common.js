module.exports = function(config) {

    return { 
        stats: {
            hash: false,
            version: false,
            timings: false,
            children: false,
            errorDetails: false,
            entrypoints: false,
            performance: config.production,
            chunks: false,
            modules: false,
            reasons: false,
            source: false,
            publicPath: false,
            builtAt: false
        },

        performance: {
            hints: false
        },

        resolve: {
            extensions: ['.tsx', '.ts', '.js', '.vue'],
            alias: {
                'vue': '@vue/runtime-dom'
            }
        }
    };
};
