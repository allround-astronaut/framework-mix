module.exports = env => {
    Object.assign(process.env, env);
    return require(process.cwd() + '/webpack.mix.js');
}