const miniCssExtractPlugin = require('./plugins/mini-css-extract');
const terserPlugin = require('./plugins/terser');
const vuePlugin = require('./plugins/vue');
const copyPlugin = require('./plugins/copy');
const cleanPlugin = require('./plugins/clean');
const deleteUnusedFilesPlugin = require('./plugins/delete-unused-files');
const notifierPlugin = require('./plugins/notifier');
const manifestPlugin = require('./plugins/manifest');
const common = require('./config/webpack.common');

class Builder {

    constructor() {
        this.config = new (require('./config'))();
    }

    entry(input, output) {
        if (this.config.entries.has(input)) {
            throw new Error(`Duplicate name "${input}". entries must be unique.`);
        }

        this.config.entries.set(input, [output]);
    }

    copy(input, output = null, ignore = [])  {
        if (this.config.copies.has(input)) {
            throw new Error(`Duplicate name "${input}". entries must be unique.`);
        }

        this.config.copies.set(input, [output, ignore]);
    }

    ignoreClean(ignore) {
        this.config.ignoreClean.push(ignore);
    }

    addManifest(manifest) {
        this.config.manifests.push(manifest);
    }

    enableVersioning(versioning = true) {
        this.config.versioning = versioning;
    }

    generate() {
        const config = common(this.config);

        config.entry = this.generateEntries();
        config.output= this.generateOutput(),
        config.module = {
            rules: this.generateRules()
        };
        config.plugins = this.generatePlugins();
        config.optimization = this.generateOptimization();

        return config;
    }

    generateEntries() {
        const entries = {};

        for (const [input, [output]] of this.config.entries) {
            entries[output.substr(0, output.lastIndexOf("."))] = input;
        }

        return entries;
    }

    generateOutput() {
        const output = {};

        output.path = this.config.paths.public;
        output.filename = this.config.versioning ? '[name].[contenthash:8].js' : '[name].js';

        return output;
    }

    generateRules() {
        let rules = [];

        rules = [...rules,
            {
                test: /\.vue$/,
                use: [
                    {
                        loader: 'vue-loader'
                    }
                ]
            },
            {
                test: /\.js$/,
                use: [
                    {
                        loader: 'babel-loader'
                    }
                ],
                exclude: [
                    /node_modules/
                ]
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: require('mini-css-extract-plugin').loader
                    },
                    {
                        loader: "css-loader",
                        options: {
                            url: false
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                config: this.config.paths.root
                            }
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            implementation: require("sass")
                        }
                    }
                ]
            }
        ];

        return rules;
    }

    generatePlugins() {
        const plugins = [];

        plugins.push(miniCssExtractPlugin(this.config));

        plugins.push(vuePlugin(this.config));

        if (this.config.copies.size > 0) {
            plugins.push(copyPlugin(this.config));
        }

        plugins.push(cleanPlugin(this.config));

        plugins.push(deleteUnusedFilesPlugin(this.config));

        plugins.push(manifestPlugin(this.config));

        plugins.push(notifierPlugin(this.config));

        return plugins;
    }

    generateOptimization() {
        const optimization = {};

        if (this.config.production) {
            optimization.minimizer = [
                terserPlugin(this.config)
            ];
        } else {
            optimization.moduleIds = 'named';
        }

        optimization.chunkIds = 'named';

        return optimization;
    }
}

module.exports = Builder;
